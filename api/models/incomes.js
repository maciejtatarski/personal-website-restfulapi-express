//income table model for the database

const mongoose = require("mongoose");

const incomesSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    value: Number,
    status: Number,
    timestamp: mongoose.Schema.Types.Date
});

module.exports = mongoose.model("Incomes", incomesSchema);
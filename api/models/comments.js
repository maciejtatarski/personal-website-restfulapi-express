//data model for the database

const mongoose = require("mongoose");

const commentSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    author: String,
    email: String,
    body: String,
    timestamp: mongoose.Schema.Types.Date
});

module.exports = mongoose.model("Comment", commentSchema);
const express = require("express");
const router = express.Router();
const Comment = require ("../models/comments"); 
const mongoose = require("mongoose");

//get method for getting comments array
router.get('/', (req, res, next) => {
    Comment
    .find()
    .then(comments =>{
        res.status(200).json({
            message:"Comments fetched",
            comments:comments
        })
    })
    .catch(err =>{
        res.status(500).json({
           error:err
        })
    })
    
}); 

//post method
router.post('/', (req,res,next) =>{
    console.log(req);
    const comment = new Comment({
        _id: new mongoose.Types.ObjectId(),
        author: req.body.author,
        email: req.body.email,
        body: req.body.body,
        timestamp: new Date().getTime()
    });
    comment
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message:"Comment created",
                comment:comment
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error:err
            });
        });
});

// requests to specyfic id
router.get("/:commentId", (req, res, next) => {
    const id = req.params.commentId;
    Comment.findById(id)
        .exec()
        .then(doc => {
            console.log(doc);
            res.status(200).json(doc);
        })
        .catch(err => {
            //console.log(err);
            res.status(500).json({error:err});
        });
}); 

router.patch("/:commentId", (req, res, next) => {
    const id = req.params.commentId;
    res.status(200).json({
        message:"Updated comment",
        id:id
    })
});

router.delete("/:commentId", (req, res, next) => {
    const id = req.params.commentId;
    res.status(200).json({
        message:"Deleted product",
        id:id
    })
});

module.exports = router;
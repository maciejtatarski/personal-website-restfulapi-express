const express = require("express");
const router = express.Router();
const Spending = require ("../models/spendings"); 
const mongoose = require("mongoose");

//get method for getting records array
router.get('/', (req, res, next) => {
    Spending
    .find()
    .then(spendings =>{
        res.status(200).json({
            message:"Spendings fetched",
            spendings:spendings
        })
    })
    .catch(err =>{
        res.status(500).json({
           error:err
        })
    })
    
}); 

//post method
router.post('/', (req,res,next) =>{
    console.log(req);
    const spending = new Spending({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        value: req.body.value,
        status: req.body.status,
        timestamp: req.body.timestamp || new Date().getTime()
    });
    spending
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message:"Record created",
                spending:spending
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error:err
            });
        });
});


module.exports = router;
const express = require("express");
const router = express.Router();
const Income = require ("../models/incomes"); 
const mongoose = require("mongoose");

//get method for getting records array
router.get('/', (req, res, next) => {
    Income
    .find()
    .then(incomes =>{
        res.status(200).json({
            message:"Incomes fetched",
            incomes:incomes
        })
    })
    .catch(err =>{
        res.status(500).json({
           error:err
        })
    })
    
}); 

//post method
router.post('/', (req,res,next) =>{
    console.log(req);
    const income = new Income({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        value: req.body.value,
        status: req.body.status,
        timestamp: req.body.timestamp || new Date().getTime()
    });
    income
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message:"Income created",
                income:income
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error:err
            });
        });
});


module.exports = router;
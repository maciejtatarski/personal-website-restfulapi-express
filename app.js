const express = require("express");
const app = express(); 
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const commentsRoutes = require("./api/routes/comments");
const spendingsRoutes = require("./api/routes/spendings");
const incomesRoutes = require("./api/routes/incomes");

//connecting to mongoDB 
mongoose.connect("mongodb+srv://admin:" + 
    process.env.MONGO_ATLAS_PASS + 
    "@testcluster.enwzd.mongodb.net/PersonalWebsite?retryWrites=true&w=majority",
    { 
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

//logs
app.use(morgan('dev')); 

app.use(bodyParser.urlencoded({extended: true})); 
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    if(res.method === "OPTIONS") {
        res.header("Access-Control-Allow-Method","PUT, POST, PATCH, DELETE, GET");
        return res.status(200);
    }
    next();
});

//request routing
app.use('/comments', commentsRoutes);
app.use('/spendings', spendingsRoutes);
app.use('/incomes', incomesRoutes);

//if request will reach this lin it means that it cannot be executed and we must return an error
app.use((req, res, next) => {
    const error = new Error('Not found!');
    error.status = 404;
    next(error); 
});

//this handle another erros
app.use((error,req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error:{
            message: error.message
        }
    });
});

module.exports = app;